//
//  NMCDManager.h
//  TNTTH
//
//  Created by Linh NGUYEN on 7/14/14.
//  Copyright (c) 2014 Nikmesoft Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NMCDManager : NSObject

@property (readonly, strong, nonatomic) NSManagedObjectContext *masterContext;
@property (readonly, strong, nonatomic) NSManagedObjectContext *mainContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (strong, nonatomic) NSURL *modelURL;
@property (strong, nonatomic) NSURL *mainStoreURL;

+ (id)shared;
- (void)saveContext;
- (void)addSQLitePersistentStore:(NSURL*)storeURL;

@end
