//
//  NMCDManager.m
//  TNTTH
//
//  Created by Linh NGUYEN on 7/14/14.
//  Copyright (c) 2014 Nikmesoft Ltd. All rights reserved.
//

#import "NMCDManager.h"
#import <CoreData/CoreData.h>

@implementation NMCDManager

@synthesize masterContext = _masterContext;
@synthesize mainContext = _mainContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;


#pragma mark - init methods
- (id)init
{
    self = [super init];
    if(self != nil)
    {
        self.modelURL = [[NSBundle mainBundle] URLForResource:@"DemoAppModel" withExtension:@"momd"];
        NSURL *documentURL = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
        
        
        self.mainStoreURL = [documentURL URLByAppendingPathComponent:@"DemoApp.sqlite"];
    }
    return self;
}

+ (id)shared
{
    static NMCDManager *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

#pragma mark - public functions
- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *mainContext = self.mainContext;
    if (mainContext != nil) {
        if ([mainContext hasChanges] && ![mainContext save:&error]) {
            DBG(@"Unresolved error %@, %@", error, [error userInfo]);
        }
        
        NSManagedObjectContext *masterCT = self.masterContext;
        if (masterCT != nil) {
            if ([masterCT hasChanges] && ![masterCT save:&error]) {
                DBG(@"Unresolved error %@, %@", error, [error userInfo]);
            }
        }
    }
}

// add new persistenStore
- (void)addSQLitePersistentStore:(NSURL*)storeURL
{
    NSError *error;
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        DBG(@"Unresolved error %@, %@", error, [error userInfo]);
    }
}

#pragma mark - Core Data stack

// Create master managed object context
- (NSManagedObjectContext *)masterContext
{
    if (_masterContext != nil) {
        return _masterContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _masterContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        [_masterContext setPersistentStoreCoordinator:coordinator];
    }
    return _masterContext;
}

// Create main managed object context
- (NSManagedObjectContext *)mainContext
{
    if (_mainContext != nil) {
        return _mainContext;
    }
    _mainContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    _mainContext.parentContext = self.masterContext;
    return _mainContext;
}

- (NSManagedObjectContext*)workerContext
{
    NSManagedObjectContext* workerContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    workerContext.parentContext = self.mainContext;
    return  workerContext;
}

// Create main managed Object Model
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    // check model URL
    if(self.modelURL == nil)
    {
        @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                       reason:@"Error, Model URL need to be configed first." userInfo:nil];
        abort();
    }
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:self.modelURL];
    return _managedObjectModel;
}

// init persistentStoreCoordinator
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // check store URL
    if(self.mainStoreURL == nil)
    {
        @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                       reason:@"Error, Main Store URL need to be configed first." userInfo:nil];
        abort();
    }
    
    DBG(@"Main PersistentStore: %@",self.mainStoreURL.path);
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:self.mainStoreURL options:nil error:&error]) {
        DBG(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}

@end
