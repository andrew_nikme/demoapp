//
//  NMOperation.m
//  NMKitFramework
//
//  Created by Linh NGUYEN on 1/3/14.
//  Copyright (c) 2014 Nikmesoft. All rights reserved.
//

#import "NMNKOperation.h"
#import "NMNKResponse.h"

@interface NMNKOperation()<NSURLConnectionDelegate>
{
    NSURLConnection *_connection;
    BOOL _completed;
    NSMutableData *_data;
    NSError *_error;
    NSDictionary *_headerFields;
    NSInteger _statusCode;
}

@property (nonatomic, strong) NSURLRequest *request;

@end

@implementation NMNKOperation

#pragma mark - init
// init
- (id)init{
    self = [super init];
    if(self)
    {
        _useNotificationCenter = DEFAULT_USE_NOTIFICATION_CENTER;
        _requestTimeout = DEFAULT_REQUEST_TIMEOUT;
        _statusCode = DEFAULT_STATUS_CODE;
    }
    return self;
}

// initWithRequest
- (id)initWithRequest:(NSURLRequest*)request
{
    self = [self init];
    if (self) {
        self.request = request;
    }
    return self;
}

// initWith URL
- (id)initWithURL:(NSURL*)url
{
    self = [self init];
    if (self) {
        NSURLRequest *_newRequest = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:_requestTimeout];
        self.request = _newRequest;
    }
    return self;
}

- (id)initWithURL:(NSURL*)url timeout:(NSTimeInterval)timeout
{
    self = [self init];
    if (self) {
        NSURLRequest *_newRequest = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:timeout];
        self.request = _newRequest;
    }
    return self;
}

- (id)initWithURL:(NSURL*)url cachePolicy:(NSURLRequestCachePolicy)cachePolicy timeout:(NSTimeInterval)timeout
{
    self = [self init];
    if (self) {
        NSURLRequest *_newRequest = [NSURLRequest requestWithURL:url cachePolicy:cachePolicy timeoutInterval:timeout];
        self.request = _newRequest;
    }
    return self;
}

// set Request
- (void)setRequest:(NSURLRequest *)request
{
    _request = request;
    if(self.identifier == nil)
    {
        self.identifier = request.URL.absoluteString;
    }
}

- (void)cancel
{
    _completed = YES;
    [_connection cancel];
    [super cancel];
}

#pragma mark - override

- (void)main
{
    _completed = NO;
    _error = nil;
    
    _connection = [NSURLConnection connectionWithRequest:self.request delegate:self];
    
    _data = [NSMutableData data];
    
    [_connection start];
    
    do {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
    } while (!_completed && !self.isCancelled);
    
    
    NMNKResponse *_response = [[NMNKResponse alloc] init];
    _response.headerFields = _headerFields;
    _response.statusCode =_statusCode;

    _response.bodyData = _data;
    
    if(_useNotificationCenter)
    {
        NSMutableDictionary *userInfo = [NSMutableDictionary dictionary];
        [userInfo setObject:_identifier forKey:@"IDENTIFIER"];
        [userInfo setObject:_response forKey:@"RESPONSE"];
        [[NSNotificationCenter defaultCenter] postNotificationName:NMNKOPERATION_NOTIFICATION_NAME object:userInfo];
    }
    
    if(_completionHandler)
    {
        _completionHandler(_error,_response);
        _completionHandler = nil;
    }
}

#pragma mark - NSURLConnectionDelegate
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [_data setLength:0];
    if(response != nil)
    {
        _headerFields = [(NSHTTPURLResponse *)response allHeaderFields];
        _statusCode = [(NSHTTPURLResponse *)response statusCode];
    } else {
        _headerFields = nil;
        _statusCode = DEFAULT_STATUS_CODE;
    }
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    _error = error;
    _completed = YES;
    _data = nil;
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [_data appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    _completed = YES;
}

@end
