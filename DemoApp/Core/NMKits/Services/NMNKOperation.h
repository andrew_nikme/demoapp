//
//  NMOperation.h
//  NMKitFramework
//
//  Created by Linh NGUYEN on 1/3/14.
//  Copyright (c) 2014 Nikmesoft. All rights reserved.
//

#import <Foundation/Foundation.h>

#pragma mark - references
@class NMNKResponse;

#pragma mark - defines

#define NMNKOPERATION_NOTIFICATION_NAME         @"NMNKOPERATION_NOTIFICATION_NAME"
#define DEFAULT_USE_NOTIFICATION_CENTER         NO
#define DEFAULT_REQUEST_TIMEOUT                 60
#define DEFAULT_STATUS_CODE                     500

#pragma mark - typedef
typedef void (^NMNKOperationHandler)(NSError* error, NMNKResponse *response);

#pragma mark - class interface

@interface NMNKOperation : NSOperation

@property (nonatomic, strong) NSString *identifier;
@property (nonatomic, copy) NMNKOperationHandler completionHandler;
@property (nonatomic, assign) BOOL useNotificationCenter;
@property (nonatomic, assign) int requestTimeout;

- (id)initWithRequest:(NSURLRequest*)request;
- (id)initWithURL:(NSURL*)url;
- (id)initWithURL:(NSURL*)url timeout:(NSTimeInterval)timeout;
- (id)initWithURL:(NSURL*)url cachePolicy:(NSURLRequestCachePolicy)cachePolicy timeout:(NSTimeInterval)timeout;

@end
