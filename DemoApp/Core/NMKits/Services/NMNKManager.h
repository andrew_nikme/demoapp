//
//  NMNKWSManager.h
//  NMKitFramework
//
//  Created by Linh NGUYEN on 3/5/14.
//  Copyright (c) 2014 Nikmesoft. All rights reserved.
//

#import <Foundation/Foundation.h>

#pragma mark - references
@class NMNKResponse;
@class NMNKOperation;

#pragma mark - defines
#define DEFAULT_MAX_CONCURRENT_OPERATION_COUNT      10


#pragma mark - typedefs
typedef void (^NMNKManagerHandler)(NSError* error, NMNKResponse *response, NSString *identifier);


typedef enum HTTPMethodType
{
    POST, GET, PUT, DELETE
} HTTPMethod;


#pragma mark - class interface

@interface NMNKManager : NSObject

@property (nonatomic, assign) NSInteger maxConcurrentOperationCount;

+ (NMNKManager *)shared;

- (NMNKOperation*)loadRequest:(NSURLRequest*)request handler:(NMNKManagerHandler)handler;

+ (NMNKOperation*)loadRequest:(NSURLRequest*)request inQueue:(NSOperationQueue*)queue handler:(NMNKManagerHandler)handler;

- (void)cancelAllOperations;

- (void)addQueue:(NMNKOperation*)operation;
- (void)setQueue:(NSOperationQueue*)newQueue;

@end
