//
//  NMNKResponse.m
//  NMKitFramework
//
//  Created by Linh NGUYEN on 3/5/14.
//  Copyright (c) 2014 Nikmesoft. All rights reserved.
//

#import "NMNKResponse.h"

@implementation NMNKResponse

#pragma mark - init
// init
- (id)init{
    self = [super init];
    if(self)
    {
        _bodyDict = [NSMutableDictionary dictionary];
    }
    return self;
}

- (void)setObject:(id)object forKey:(NSString*)key
{
    [_bodyDict setObject:object forKey:key];
}

@end
