//
//  NMNKDownloader.h
//  NMKitFramework
//
//  Created by Linh NGUYEN on 4/3/14.
//  Copyright (c) 2014 Nikmesoft. All rights reserved.
//

#import <Foundation/Foundation.h>

#pragma mark - references
@class NMNKResponse;
@class NMNKOperation;

#pragma mark - defines
#define DEFAULT_MAX_CONCURRENT_OPERATION_COUNT      10


#pragma mark - typedefs
typedef void (^NMNKDownloaderHandler)(NSError* error, NMNKResponse *response, NSString *identifier, NSInteger index);


#pragma mark - class interface

@interface NMNKDownloader : NSObject

@property (nonatomic, assign) NSInteger maxConcurrentOperationCount;

+ (NMNKDownloader *)sharedInstance;

- (NMNKOperation*)loadRequest:(NSURLRequest*)request index:(NSInteger)index handler:(NMNKDownloaderHandler)handler;
+ (NMNKOperation*)loadRequest:(NSURLRequest*)request index:(NSInteger)index inQueue:(NSOperationQueue*)queue handler:(NMNKDownloaderHandler)handler;

- (void)cancelAllOperations;
- (void)cancelOperation:(NSString*)identifier;

- (void)addQueue:(NMNKOperation*)operation;
- (void)setQueue:(NSOperationQueue*)newQueue;

@end
