//
//  NMNKDownloader.m
//  NMKitFramework
//
//  Created by Linh NGUYEN on 4/3/14.
//  Copyright (c) 2014 Nikmesoft. All rights reserved.
//

#import "NMNKDownloader.h"
#import "NMNKOperation.h"
#import "NMNKResponse.h"

@interface NMNKDownloader()
{
    NSOperationQueue *_nmQueue;
}
@end

@implementation NMNKDownloader

#pragma mark - init methods
- (id)init
{
    self = [super init];
    if (self) {
        _maxConcurrentOperationCount = DEFAULT_MAX_CONCURRENT_OPERATION_COUNT;
        _nmQueue = [[NSOperationQueue alloc] init];
        _nmQueue.maxConcurrentOperationCount = _maxConcurrentOperationCount;
    }
    return self;
}

- (void)dealloc
{
    
}

+ (NMNKDownloader *)sharedInstance
{
    static NMNKDownloader *instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

#pragma mark - load requests
// load a request in another queue by user
+ (NMNKOperation*)loadRequest:(NSURLRequest*)request index:(NSInteger)index inQueue:(NSOperationQueue*)queue handler:(NMNKDownloaderHandler)handler
{
    NMNKOperation *_newOperation = [[NMNKOperation alloc] initWithRequest:request];
    __block NSString *_identifier = _newOperation.identifier;
    _newOperation.completionHandler = ^(NSError* error, NMNKResponse *response)
    {
        if(handler != nil)
        {
            handler(error,response,_identifier,index);
        }
    };
    [queue addOperation:_newOperation];
    return _newOperation;
}

// load a request by common queue
- (NMNKOperation*)loadRequest:(NSURLRequest*)request index:(NSInteger)index handler:(NMNKDownloaderHandler)handler
{
    NMNKOperation *_newOperation = [[NMNKOperation alloc] initWithRequest:request];
    __block NSString *_identifier= _newOperation.identifier;
    [self cancelOperation:_newOperation.identifier];
    
    _newOperation.completionHandler = ^(NSError* error, NMNKResponse *response)
    {
        if(handler != nil)
        {
            handler(error,response,_identifier,index);
        }
    };
    [_nmQueue addOperation:_newOperation];
    return _newOperation;
}


#pragma mark - cancel all request in queue
- (void)cancelAllOperations
{
    [_nmQueue cancelAllOperations];
}

- (void)cancelOperation:(NSString*)identifier
{
    for (NMNKOperation *op in _nmQueue.operations) {
        if(op && [op.identifier isEqualToString:identifier])
        {
            [op cancel];
        }
    }
}

#pragma mark - add a operation to queue
- (void)addQueue:(NMNKOperation*)operation
{
    [_nmQueue addOperation:operation];
}

- (void)setQueue:(NSOperationQueue*)newQueue
{
    if(newQueue)
    {
        _nmQueue = newQueue;
    }
}

- (void)setMaxConcurrentOperationCount:(NSInteger)maxConcurrentOperationCount
{
    _maxConcurrentOperationCount = maxConcurrentOperationCount;
    _nmQueue.maxConcurrentOperationCount = _maxConcurrentOperationCount;
}
@end

