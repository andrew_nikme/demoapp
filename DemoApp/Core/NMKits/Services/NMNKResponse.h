//
//  NMNKResponse.h
//  NMKitFramework
//
//  Created by Linh NGUYEN on 3/5/14.
//  Copyright (c) 2014 Nikmesoft. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NMNKResponse : NSObject

@property (nonatomic, assign) NSInteger statusCode;
@property (nonatomic, strong) NSDictionary *headerFields;
@property (nonatomic, strong) NSData *bodyData;
@property (nonatomic, strong) NSMutableDictionary *bodyDict;

- (void)setObject:(id)object forKey:(NSString*)key;

@end
