//
//  NMSoundManager.m
//  TNTTH
//
//  Created by Linh NGUYEN on 7/14/14.
//  Copyright (c) 2014 Nikmesoft Ltd. All rights reserved.
//

#import "NMSoundHelper.h"
#import <AudioToolbox/AudioServices.h>
#import <AVFoundation/AVFoundation.h>


#pragma mark - SoundEffect class

@interface SoundEffect : NSObject

@property (nonatomic) SystemSoundID systemSoundID;
@property (nonatomic, strong) NSString *soundName;

- (id)initWithID:(SystemSoundID)systemSoundID name:(NSString*)soundName;

@end

@implementation SoundEffect

- (id)initWithID:(SystemSoundID)systemSoundID name:(NSString*)soundName
{
    self = [self init];
    if(self)
    {
        self.systemSoundID = systemSoundID;
        self.soundName = soundName;
    }
    return self;
}

- (void)dealloc
{
    
}

@end


@interface NMSoundHelper()
{
    NSMutableDictionary *_soundDict;
    AVAudioPlayer *_audioPlayer;
}

@end

@implementation NMSoundHelper

- (id) init
{
    if(nil != (self = [super init]))
    {
        _soundDict = [[NSMutableDictionary alloc] init];
        _paused = NO;
        _keepInMemory = NO;
    }
    return self;
}


+ (id)shared {
    static NMSoundHelper *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

- (void)preloadEffect:(NSString*)soundName
{
    if([_soundDict valueForKey:soundName] == nil)
    {
        SystemSoundID soundID;
        NSString *soundPath = [[NSBundle mainBundle] pathForResource:soundName ofType:nil];
        CFURLRef soundUrl = (__bridge CFURLRef) [NSURL fileURLWithPath:soundPath];
        //Use audio sevices to create the sound
        AudioServicesCreateSystemSoundID((CFURLRef)soundUrl, &soundID);
        CFRelease(soundUrl);
        
        SoundEffect *soundEffect = [[SoundEffect alloc] initWithID:soundID name:soundName];
        [_soundDict setObject:soundEffect forKey:soundName];
    }
    
    
}

- (void)playBg:(NSString*)soundName loop:(BOOL)loop
{
    NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/%@", [[NSBundle mainBundle] resourcePath],soundName]];
	
	NSError *error;
	_audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
    if(loop)
    {
        _audioPlayer.numberOfLoops = -1;
    }
	if (_audioPlayer != nil)
    {
        [_audioPlayer play];
    }
    
}

- (void)playEffect:(NSString*)soundName
{
    if(_keepInMemory)
    {
        if([_soundDict valueForKey:soundName] == nil)
        {
            [self preloadEffect:soundName];
        }
        SoundEffect *soundEffect = [_soundDict objectForKey:soundName];
        if(soundEffect && soundEffect.systemSoundID)
        {
            AudioServicesPlaySystemSound(soundEffect.systemSoundID);
        }
    } else {
        SystemSoundID soundID;
        NSString *soundPath = [[NSBundle mainBundle] pathForResource:soundName ofType:nil];
        CFURLRef soundUrl = (__bridge CFURLRef) [NSURL fileURLWithPath:soundPath];
        //Use audio sevices to create the sound
        AudioServicesCreateSystemSoundID((CFURLRef)soundUrl, &soundID);
        CFRelease(soundUrl);
        AudioServicesPlaySystemSound (soundID);
    }
    
}

- (void)setPaused:(BOOL)paused
{
    _paused = paused;
    
    if(paused)
    {
        if(_audioPlayer && _audioPlayer.isPlaying)
        {
            [_audioPlayer pause];
        }
    } else {
        if(_audioPlayer && _audioPlayer.isPlaying)
        {
            [_audioPlayer play];
        }
    }
}

- (void)setKeepInMemory:(BOOL)keepInMemory
{
    _keepInMemory = keepInMemory;
    if(keepInMemory == NO)
    {
        [_soundDict removeAllObjects];
    }
}

- (void)stopEverything
{
    if(_audioPlayer)
    {
        [_audioPlayer stop];
    }
    _paused = TRUE;
}

- (void)unloadAllEffects
{
    if(_audioPlayer)
    {
        [_audioPlayer stop];
        _audioPlayer = nil;
    }
    if(_soundDict)
    {
        [_soundDict removeAllObjects];
    }
    
    _paused = TRUE;
    
}


@end