//
//  NMUIHelper.h
//  TNTTH
//
//  Created by Linh NGUYEN on 7/14/14.
//  Copyright (c) 2014 Nikmesoft Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NMUIHelper : NSObject

+ (void)showAlertWithTitle:(NSString *)title message:(NSString *)message delegate:(id)delegate tag:(NSInteger)tag
         cancelButtonTitle:(NSString *)cancelButtonTitle;

+ (void)showConfirmDialogWithTitle:(NSString *)title message:(NSString *)message delegate:(id)delegate tag:(NSInteger)tag
                     noButtonTitle:(NSString *)noButtonTitle yesButtonTitle:(NSString*)yesButtonTitle;

@end
