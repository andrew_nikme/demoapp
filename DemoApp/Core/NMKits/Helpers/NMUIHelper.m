//
//  NMUIHelper.m
//  TNTTH
//
//  Created by Linh NGUYEN on 7/14/14.
//  Copyright (c) 2014 Nikmesoft Ltd. All rights reserved.
//

#import "NMUIHelper.h"

@implementation NMUIHelper

+ (void)showAlertWithTitle:(NSString *)title message:(NSString *)message delegate:(id)delegate tag:(NSInteger)tag
         cancelButtonTitle:(NSString *)cancelButtonTitle
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:message delegate:delegate cancelButtonTitle:cancelButtonTitle otherButtonTitles:nil];
    alertView.tag = tag;
    [alertView show];
    alertView = nil;
}

+ (void)showConfirmDialogWithTitle:(NSString *)title message:(NSString *)message delegate:(id)delegate tag:(NSInteger)tag
                     noButtonTitle:(NSString *)noButtonTitle yesButtonTitle:(NSString*)yesButtonTitle
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:message delegate:delegate cancelButtonTitle:noButtonTitle otherButtonTitles:yesButtonTitle, nil];
    alertView.tag = tag;
    [alertView show];
    alertView = nil;
}

@end
