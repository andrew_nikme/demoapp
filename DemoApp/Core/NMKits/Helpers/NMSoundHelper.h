//
//  NMSoundManager.h
//  TNTTH
//
//  Created by Linh NGUYEN on 7/14/14.
//  Copyright (c) 2014 Nikmesoft Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NMSoundHelper : NSObject

@property (nonatomic) BOOL paused;

@property (nonatomic) BOOL keepInMemory;

+ (id)shared;

- (void)preloadEffect:(NSString*)soundName;

- (void)playBg:(NSString*)soundName loop:(BOOL)loop;

- (void)playEffect:(NSString*)soundName;

- (void)setPaused:(BOOL)paused;

- (void)stopEverything;

- (void)unloadAllEffects;

@end