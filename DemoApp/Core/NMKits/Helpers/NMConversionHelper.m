//
//  NMConversionHelper.m
//  DemoApp
//
//  Created by Linh NGUYEN on 7/19/14.
//  Copyright (c) 2014 Nikmesoft Ltd. All rights reserved.
//

#import "NMConversionHelper.h"
#import <CommonCrypto/CommonDigest.h>
#include <CommonCrypto/CommonHMAC.h>
#import <objc/runtime.h>

@implementation NMConversionHelper

+ (NSString *)md5FromString:(NSString *)source
{
    const char *src = [source UTF8String];
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CC_MD5(src, (int)strlen(src), result);
    NSString *ret = [[NSString alloc] initWithFormat:@"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
                     result[0], result[1], result[2], result[3],
                     result[4], result[5], result[6], result[7],
                     result[8], result[9], result[10], result[11],
                     result[12], result[13], result[14], result[15]
                     ];
    return [ret lowercaseString];
}

+ (NSString*)formatDate:(NSDate*)date format:(NSString*)format
{
    if(date == nil) return @"";
    NSDateFormatter *formater = [[NSDateFormatter alloc] init];
    [formater setDateFormat:format];
    return [formater stringFromDate:date];
}

+ (NSDictionary *)dictionaryWithPropertiesOfObject:(id)obj
{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    
    unsigned count;
    objc_property_t *properties = class_copyPropertyList([obj class], &count);
    
    for (int i = 0; i < count; i++) {
        NSString *key = [NSString stringWithUTF8String:property_getName(properties[i])];
        Class classObject = NSClassFromString([key capitalizedString]);
        if (classObject) {
            id subObj = [self dictionaryWithPropertiesOfObject:[obj valueForKey:key]];
            [dict setObject:subObj forKey:key];
        }
        else
        {
            id value = [obj valueForKey:key];
            if(value) [dict setObject:value forKey:key];
        }
    }
    
    free(properties);
    
    return [NSDictionary dictionaryWithDictionary:dict];
}

+ (NSString*)hashMAC256:(NSString*)data key:(NSString*)key
{
    const char *privateKey  = [key cStringUsingEncoding:NSUTF8StringEncoding];
    const char *requestData = [data cStringUsingEncoding:NSUTF8StringEncoding];
    unsigned char cHMAC[CC_SHA256_DIGEST_LENGTH];
    CCHmac(kCCHmacAlgSHA256, // algorithm
           privateKey, strlen(privateKey), // privateKey
           requestData, strlen(requestData), // requestData
           cHMAC); // length
    
    NSMutableString* output = [NSMutableString stringWithCapacity:CC_SHA256_DIGEST_LENGTH * 2];
    for(int i = 0; i < CC_SHA256_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x", cHMAC[i]];
    
    return [NMConversionHelper base64String:output];
}

+ (NSString *)base64String:(NSString *)str
{
    NSData *theData = [str dataUsingEncoding: NSASCIIStringEncoding];
    const uint8_t* input = (const uint8_t*)[theData bytes];
    NSInteger length = [theData length];
    
    static char table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    
    NSMutableData* data = [NSMutableData dataWithLength:((length + 2) / 3) * 4];
    uint8_t* output = (uint8_t*)data.mutableBytes;
    
    NSInteger i;
    for (i=0; i < length; i += 3) {
        NSInteger value = 0;
        NSInteger j;
        for (j = i; j < (i + 3); j++) {
            value <<= 8;
            
            if (j < length) {
                value |= (0xFF & input[j]);
            }
        }
        
        NSInteger theIndex = (i / 3) * 4;
        output[theIndex + 0] =                    table[(value >> 18) & 0x3F];
        output[theIndex + 1] =                    table[(value >> 12) & 0x3F];
        output[theIndex + 2] = (i + 1) < length ? table[(value >> 6)  & 0x3F] : '=';
        output[theIndex + 3] = (i + 2) < length ? table[(value >> 0)  & 0x3F] : '=';
    }
    
    return [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
}

+ (char *)xor:(NSData *)data withKey:(NSString *)key
{
    // input
    NSUInteger inputLength = [data length];
    unsigned char *inputBytes = malloc( inputLength * sizeof(unsigned char) );
    [data getBytes:inputBytes length:inputLength];
    
    //key
    NSData *keyData = [key dataUsingEncoding:NSASCIIStringEncoding];
    NSUInteger keyLength = [keyData length];
    unsigned char *keyBytes = malloc( keyLength * sizeof(unsigned char) );
    [keyData getBytes:keyBytes length:keyLength];
    
    char *outputBytes = malloc( inputLength * sizeof(unsigned char) );
    for (int i =0;i< inputLength; i++) {
        outputBytes[i] = inputBytes[i] ^ keyBytes[i % keyLength];
    }
    return outputBytes;
    
}

@end
