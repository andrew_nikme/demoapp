//
//  NMConversionHelper.h
//  DemoApp
//
//  Created by Linh NGUYEN on 7/19/14.
//  Copyright (c) 2014 Nikmesoft Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NMConversionHelper : NSObject

+ (NSString*)formatDate:(NSDate*)date format:(NSString*)format;
+ (NSString *)md5FromString:(NSString *)source;
+ (NSDictionary *)dictionaryWithPropertiesOfObject:(id)obj;
+ (NSString*)hashMAC256:(NSString*)data key:(NSString*)key;
+ (NSString *)base64String:(NSString *)str;
+ (char *)xor:(NSData *)data withKey:(NSString *)key;

@end
