//
//  NMViewController.h
//  TNTTH
//
//  Created by Linh NGUYEN on 7/14/14.
//  Copyright (c) 2014 Nikmesoft Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

#define m_isiPhone5  ([[UIScreen mainScreen] bounds].size.height == 568)?TRUE:FALSE
#define m_isiPhone  (UI_USER_INTERFACE_IDIOM() == 0)?TRUE:FALSE

@interface NMViewController : UIViewController

@end
