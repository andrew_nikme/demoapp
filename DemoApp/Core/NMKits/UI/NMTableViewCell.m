//
//  NMTableViewCell.m
//  TNTTH
//
//  Created by Linh NGUYEN on 7/14/14.
//  Copyright (c) 2014 Nikmesoft Ltd. All rights reserved.
//

#import "NMTableViewCell.h"

@implementation NMTableViewCell

- (id)init
{
    if ([self class] == [NMTableViewCell class]) {
        @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                       reason:@"Error, attempting to instantiate AbstractClass directly." userInfo:nil];
    }
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
    }
    [self viewDidLoad];
    return self;
}

- (id)initWithNibName:(NSString*)nibName
{
    self = [super init];
    if(self)
    {
        if(nibName == nil)
        {
            nibName = NSStringFromClass([self class]);
        }
        NSArray *arrayOfViews = [[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil];
        if([arrayOfViews count] > 0) {
            for (id v in arrayOfViews) {
                if([v isKindOfClass:[self class]])
                {
                    self = v;
                }
            }
        }
    }
    [self viewDidLoad];
    
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

#pragma mark - init
- (void)viewDidLoad
{
    @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                   reason:[NSString stringWithFormat:@"You must override %@ in a subclass",
                                           NSStringFromSelector(_cmd)]
                                 userInfo:nil];
}

@end
