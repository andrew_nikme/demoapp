//
//  NMTableViewCell.h
//  TNTTH
//
//  Created by Linh NGUYEN on 7/14/14.
//  Copyright (c) 2014 Nikmesoft Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NMTableViewCell : UITableViewCell

- (id)initWithNibName:(NSString*)nibName;
- (void)viewDidLoad;


@end
