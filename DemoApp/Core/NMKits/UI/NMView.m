//
//  NMView.m
//  TNTTH
//
//  Created by Linh NGUYEN on 7/14/14.
//  Copyright (c) 2014 Nikmesoft Ltd. All rights reserved.
//

#import "NMView.h"

@implementation NMView

#pragma mark
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    [self viewDidLoad];
    return self;
}

- (id)init
{
    if ([self class] == [NMView class]) {
        @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                       reason:@"Error, attempting to instantiate AbstractClass directly." userInfo:nil];
    }
    self = [super init];
    if (self) {
    }
    return self;
}

- (id)initWithNibName:(NSString*)nibName
{
    self = [super init];
    if(self)
    {
        if(nibName == nil)
        {
            nibName = NSStringFromClass([self class]);
        }
        NSArray *arrayOfViews = [[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil];
        if([arrayOfViews count] > 0) {
            for (id v in arrayOfViews) {
                if([v isKindOfClass:[self class]])
                {
                    self = v;
                }
            }
        }
    }
    [self viewDidLoad];
    
    return self;
}

#pragma mark - init
- (void)viewDidLoad
{
    @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                   reason:[NSString stringWithFormat:@"You must override %@ in a subclass",
                                           NSStringFromSelector(_cmd)]
                                 userInfo:nil];
}

#pragma mark - Show/Hide Method
- (void)showInView:(UIView*)parentView animation:(UIViewAnimationOptions)animation
          duration:(NSTimeInterval)duration
{
    [UIView transitionWithView:parentView duration:duration options:animation animations:^{
        [parentView addSubview:self];
    } completion:^(BOOL finished) {
        
    }];
}

- (void)dismissWithAnimation:(UIViewAnimationOptions)animation duration:(NSTimeInterval)duration
{
    [UIView animateWithDuration:duration delay:0.0 options:animation
                     animations:^{self.alpha = 0.0;}
                     completion:^(BOOL finished){ [self removeFromSuperview]; }];
}


@end
