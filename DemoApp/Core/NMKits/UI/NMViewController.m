//
//  NMViewController.m
//  TNTTH
//
//  Created by Linh NGUYEN on 7/14/14.
//  Copyright (c) 2014 Nikmesoft Ltd. All rights reserved.
//

#import "NMViewController.h"

@interface NMViewController ()

@end

@implementation NMViewController

- (id)init
{
    NSString *nibName = NSStringFromClass([self class]);
    
    if ([[NSBundle mainBundle] pathForResource:nibName ofType:@"nib"])
    {
        NSString *suffix  = (m_isiPhone == TRUE) ? ((m_isiPhone5 == TRUE) ? @"-568h" : @"") : @"~ipad";
        NSString *nibNameSuffix = [NSString stringWithFormat:@"%@%@", nibName, suffix];
        
        if ([[NSBundle mainBundle] pathForResource:nibNameSuffix ofType:@"nib"]) {
            nibName = nibNameSuffix;
        }
        
        self = [super initWithNibName:nibName bundle:nil];
        if (self) {
        }
        return self;
    }
    else {
        self = [super init];
        if (self) {
        }
        return self;
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
#if __IPHONE_OS_VERSION_MAX_ALLOWED > __IPHONE_6_1
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
#endif
}
@end
