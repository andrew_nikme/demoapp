//
//  NMView.h
//  TNTTH
//
//  Created by Linh NGUYEN on 7/14/14.
//  Copyright (c) 2014 Nikmesoft Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NMView : UIView

- (id)initWithNibName:(NSString*)nibName;

- (void)showInView:(UIView*)parentView animation:(UIViewAnimationOptions)animation
          duration:(NSTimeInterval)duration;

- (void)dismissWithAnimation:(UIViewAnimationOptions)animation duration:(NSTimeInterval)duration;

- (void)viewDidLoad;

@end
