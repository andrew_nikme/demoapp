//
//  RootViewController.m
//  TNTTH
//
//  Created by Linh NGUYEN on 7/14/14.
//  Copyright (c) 2014 Nikmesoft Ltd. All rights reserved.
//

#import "RootViewController.h"
#import <FacebookSDK/FacebookSDK.h>
#import "WSManager.h"
#import "MBProgressHUD.h"
#import "WSError.h"
#import "NMNKResponse.h"
#import "NMUIHelper.h"

@interface RootViewController ()<FBLoginViewDelegate>
{
    __weak IBOutlet FBLoginView *_fbLoginView;
}

@end

@implementation RootViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = NSLocalizedString(@"APP_NAME", nil);
    [_fbLoginView setReadPermissions:@[@"public_profile", @"user_birthday"]];
    [_fbLoginView setDelegate:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - FBLoginViewDelegate
- (void)loginViewShowingLoggedInUser:(FBLoginView *)loginView
{
    
}

- (void)loginViewFetchedUserInfo:(FBLoginView *)loginView
                            user:(id<FBGraphUser>)user
{
    NSString *device_udid = [[UIDevice currentDevice] identifierForVendor].UUIDString;
    NSMutableDictionary *userDict = [NSMutableDictionary dictionary];
    [userDict setObject:device_udid forKey:@"device_uuid"];
    [userDict setObject:user.id forKey:@"open_uuid"];
    [userDict setObject:user.name forKey:@"name"];
    [self wsSocialAuth:userDict];
}

- (void)loginView:(FBLoginView *)loginView
      handleError:(NSError *)error
{
    NSString *alertMessage, *alertTitle;
    
    if ([FBErrorUtility shouldNotifyUserForError:error]) {
        alertTitle = @"Facebook error";
        alertMessage = [FBErrorUtility userMessageForError:error];
    } else if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryAuthenticationReopenSession) {
        alertTitle = @"Session Error";
        alertMessage = @"Your current session is no longer valid. Please log in again.";
    } else if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryUserCancelled) {
        NSLog(@"user cancelled login");
    } else {
        alertTitle  = @"Something went wrong";
        alertMessage = @"Please try again later.";
        NSLog(@"Unexpected error:%@", error);
    }
    
    if (alertMessage) {
        [NMUIHelper showAlertWithTitle:alertTitle message:alertMessage delegate:nil tag:0 cancelButtonTitle:@"OK"];
    }
}

#pragma mark - WS Services
- (void)wsSocialAuth:(NSDictionary*)userData
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSData *postData = [NSJSONSerialization dataWithJSONObject:userData options:NSJSONWritingPrettyPrinted error:nil];
    [WSManager socialAuth:postData handler:^(WSError *error, NMNKResponse *response, NSString *identifier) {
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        }];
        if(error)
        {
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                [NMUIHelper showAlertWithTitle:@"DemoApp" message:error.message delegate:nil tag:0 cancelButtonTitle:@"OK"];
            }];
        } else {
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                [NMUIHelper showAlertWithTitle:@"DemoApp" message:@"Logined successfully!" delegate:nil tag:0 cancelButtonTitle:@"OK"];
            }];
        }
    }];
}
@end
