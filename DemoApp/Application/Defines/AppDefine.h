//
//  AppDefine.h
//  TNTTH
//
//  Created by Linh NGUYEN on 7/14/14.
//  Copyright (c) 2014 Nikmesoft Ltd. All rights reserved.
//

#ifndef TNTTH_AppDefine_h
#define TNTTH_AppDefine_h

//////////////////////////////////////////////////////
// WS Defines

#define WS_APPLICATION_KEY              @"NM_MOBILE_IOS_V1000"
#define WS_PRIVATE_KEY                  @"abcde12345-"

#define WS_METHOD_SOCIAL_AUTH           @"http://nikmesoft.com/apis/DemoAPI/api.php/users/auth/social"

#endif
