//
//  WSManager.h
//  DemoApp
//
//  Created by Linh NGUYEN on 7/19/14.
//  Copyright (c) 2014 Nikmesoft Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@class NMNKResponse;
@class WSError;

@interface WSManager : NSObject

typedef void (^WSManagerHandler)(WSError *error, NMNKResponse *response, NSString *identifier);

+ (void)socialAuth:(NSData*)data handler:(WSManagerHandler)handler;

@end
