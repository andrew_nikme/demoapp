//
//  WSError.h
//  DemoApp
//
//  Created by Linh NGUYEN on 7/19/14.
//  Copyright (c) 2014 Nikmesoft Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@class NMNKResponse;

@interface WSError : NSObject

@property (nonatomic, assign) NSString *code;
@property (nonatomic, assign) NSString *message;

- (id)initWithCode:(NSString*)code message:(NSString*)message;
+ (WSError*)commonError;
+ (WSError*)errorFromResponse:(NMNKResponse*)response;

@end
