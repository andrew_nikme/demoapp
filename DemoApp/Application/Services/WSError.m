//
//  WSError.m
//  DemoApp
//
//  Created by Linh NGUYEN on 7/19/14.
//  Copyright (c) 2014 Nikmesoft Ltd. All rights reserved.
//

#import "WSError.h"
#import "NMNKResponse.h"

#define DEFAULT_CODE             @"WS_COMMON_CODE"

#define CODE_TAG                 @"nm-code"
#define MESSAGE_TAG              @"nm-message"



@implementation WSError


#pragma mark - init

- (id)init
{
    self = [super init];
    if(self)
    {
        _code = DEFAULT_CODE;
        _message = DEFAULT_CODE;
    }
    return self;
}

- (id)initWithCode:(NSString*)code message:(NSString*)message
{
    self = [self init];
    if(self)
    {
        _code = code;
        _message = message;
    }
    return self;
}


#pragma mark - static methods
+ (WSError*)commonError
{
    WSError *error = [[WSError alloc] init];
    error.code = DEFAULT_CODE;
    error.message = DEFAULT_CODE;
    return error;
}

+ (WSError*)errorFromResponse:(NMNKResponse*)response
{
    if(response != nil && response.statusCode >= 400)
    {
        NSDictionary *headerDict = response.headerFields;
        if(headerDict && [headerDict valueForKey:CODE_TAG] && [headerDict valueForKey:MESSAGE_TAG])
        {
            NSString *code = [headerDict valueForKey:CODE_TAG];
            NSString *message = [headerDict valueForKey:MESSAGE_TAG];
            return [[WSError alloc] initWithCode:code message:message];
        } else {
            return [WSError commonError];
        }
    }
    return nil;
}


@end
