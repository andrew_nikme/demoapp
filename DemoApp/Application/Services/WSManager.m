//
//  WSManager.m
//  DemoApp
//
//  Created by Linh NGUYEN on 7/19/14.
//  Copyright (c) 2014 Nikmesoft Ltd. All rights reserved.
//

#import "WSManager.h"
#import "NMNetworkKit.h"
#import "NMConversionHelper.h"
#import "AppDefine.h"
#import "WSError.h"

@implementation WSManager

#pragma mark - init

static NMNKManager *serviceQueue = nil;
static NMNKManager *downloadQueue = nil;

+ (NSString*)method:(HTTPMethod)method
{
    switch (method) {
        case POST:
            return @"POST";
        case GET:
            return @"GET";
        case PUT:
            return @"PUT";
        case DELETE:
            return @"DELETE";
        default:
            return @"";
    }
}

#pragma mark - queues
+ (NMNKManager*)serviceQueue
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        serviceQueue = [[NMNKManager alloc] init];
        serviceQueue.maxConcurrentOperationCount = 1;
    });
    return serviceQueue;
}

+ (NMNKManager*)downloadQueue
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        downloadQueue = [[NMNKManager alloc] init];
        downloadQueue.maxConcurrentOperationCount = 6;
    });
    return downloadQueue;
}

#pragma mark - create requests
// auth request for general methods
+ (NSMutableURLRequest*)createAuthRequest:(NSString*)url body:(NSData*)bodyData httpMethod:(HTTPMethod)method
{
    // get UTC timestamp
    NSTimeInterval timestamp = [[NSDate date] timeIntervalSince1970];
    // general singnature
    NSString *bodyString = @"";
    if(bodyData != nil)
    {
        bodyString = [[NSString alloc] initWithData:bodyData encoding:NSUTF8StringEncoding];
    }
    NSString *string_to_sign = [NSString stringWithFormat:@"%@&%@&%f&%@",[WSManager method:method],url,timestamp,bodyString];
    NSLog(@"iOS %@",string_to_sign);
    NSString *signature = [NMConversionHelper hashMAC256:string_to_sign key:WS_PRIVATE_KEY];
    NSLog(@"iOS %@",signature);
    // create request
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:url]];
    [request setHTTPMethod:[WSManager method:method]];
    [request setHTTPBody:bodyData];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:WS_APPLICATION_KEY forHTTPHeaderField:@"APPLICATION-KEY"];
    [request setValue:[NSString stringWithFormat:@"%f",timestamp] forHTTPHeaderField:@"TIMESTAMP"];
    [request setValue:signature forHTTPHeaderField:@"SIGNATURE"];
    
    return request;
}

#pragma mark - load common request
+ (void)callService:(NSMutableURLRequest*)request handler:(WSManagerHandler)handler
{
    DBG(@"NM-WS-REQUEST-URL: %@",request.URL.absoluteString);
    DBG(@"NM-WS-REQUEST-METHOD: %@",request.HTTPMethod);
    DBG(@"NM-WS-REQUEST-BODY: %@",[[NSString alloc] initWithData:request.HTTPBody encoding:NSUTF8StringEncoding]);
    [[WSManager serviceQueue] loadRequest:request handler:^(NSError *error, NMNKResponse *response, NSString *identifier) {
        if(handler != nil)
        {
            handler([WSError errorFromResponse:response],response,identifier);
        }
    }];
}

#pragma mark - WS methods
+ (void)socialAuth:(NSData*)data handler:(WSManagerHandler)handler
{
    NSString *strURL = [NSString stringWithFormat:@"%@",WS_METHOD_SOCIAL_AUTH];
    NSMutableURLRequest *request = [WSManager createAuthRequest:strURL body:data httpMethod:POST];
    [WSManager callService:request handler:^(WSError *error, NMNKResponse *response, NSString *identifier) {
        if(!error && response.bodyData != nil)
        {
            
        }
        if(handler != nil)
        {
            handler(error,response,identifier);
        }
        
    }];
}
@end
